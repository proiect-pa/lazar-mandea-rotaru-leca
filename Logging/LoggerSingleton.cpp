#include "LoggerSingleton.h"

Logger& LoggerSingleton::getLoggerInstance(const std::string& filePath)
{
	static Logger log(filePath);
	
	return log;
}
