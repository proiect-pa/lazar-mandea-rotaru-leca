#pragma once


#include "LoggingExports.h"

#include "Logger.h"


class LOGGING_API LoggerSingleton
{

public:
	LoggerSingleton(LoggerSingleton const&) = delete;             // Copy construct
	LoggerSingleton(LoggerSingleton&&) = delete;                  // Move construct
	LoggerSingleton& operator=(LoggerSingleton const&) = delete;  // Copy assign
	LoggerSingleton& operator=(LoggerSingleton&&) = delete;      // Move assign

	static Logger& getLoggerInstance(const std::string&);
	
		
		
		

protected:
	LoggerSingleton() = default;
	~LoggerSingleton() = default;

};


