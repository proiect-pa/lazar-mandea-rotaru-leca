#pragma once
#include <string>
#include <iostream>

#include "LoggingExports.h"

#include <fstream>


	class LOGGING_API Logger
	{
	public:
		enum class Level
		{
			Info,
			Warning,
			Error
		};

	public:
	

		Logger(const std::string& filename);

		void Log(const std::string& message, Level level);
	

		

		~Logger()=default;

	private:

	
		std::ostream& m_os;
		std::ofstream m_fout;

		
	};



	
