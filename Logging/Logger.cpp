#include "Logger.h"


#include <iomanip>
#include <time.h>




Logger::Logger(const std::string& filename) : m_fout(filename), m_os(m_fout)
{
}

void Logger::Log(const std::string& message, Level level)
{

	
	switch (level) {

	case Level::Info:
		m_os << "[INFO]";
		break;

	case Level::Warning:
		m_os << "[WARNING]";
		break;

	case Level::Error:
		m_os << "[ERROR]";
		break;
	}
	std::time_t crtTime = std::time(nullptr);
	m_os << '[' << std::put_time(localtime(&crtTime), "%Y-%m-%d %H:%M:%S") << ']';

	
	m_os << " " << message << std::endl;
}







