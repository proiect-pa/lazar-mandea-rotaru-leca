#pragma once

#include "Cell.h"

class CellHash {

public:
    size_t operator()(const std::shared_ptr<Cell>&) const;



private:
    std::hash<uint8_t> m_hash;




};
