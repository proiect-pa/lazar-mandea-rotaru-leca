#pragma once

#include "Cell.h"
class EqualFunctionCell
{

public:


	inline bool operator ()(const std::shared_ptr<Cell>&, const std::shared_ptr<Cell>&) const;

};



inline bool EqualFunctionCell::operator() (const std::shared_ptr<Cell>& firstPtr, const std::shared_ptr<Cell>& secondPtr) const {

	if (firstPtr->GetLine() == secondPtr->GetLine() && firstPtr->GetColumn() == secondPtr->GetColumn())
		return true;
	return false;
}