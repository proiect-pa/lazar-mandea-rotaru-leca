#include "Cell.h"

//Cell::Cell() : m_cellType(CellType::Clear), m_line(0u), m_column(0u) {}






Cell::Cell(const uint8_t line, const uint8_t column) :m_cellType(CellType::Clear), m_line(line), m_column(column), m_isFlagged(false)
{
}



Cell::Cell(const Cell& cell)
{
	*this = cell;
	SetAsUnflagged();
}







Cell::Cell(const std::shared_ptr<Cell>& other)

{	
	//if(other!=NULL)
	 *this = other;


	 SetAsUnflagged();
}



Cell& Cell::operator=(const Cell& other)
{
	if (this == &other)

		return *this;

	if (m_cellType != other.m_cellType) {
		this->m_cellType = other.m_cellType;


	}

	if (m_line!= other.m_line) {
		this->m_line = other.m_line;
	}
	if (m_column != other.m_column) {
		this->m_column = other.m_column;
	}

	return *this;
}


Cell& Cell::operator=(const std::shared_ptr<Cell>& other)
{
	const auto& otherCell = other.get();

	if (otherCell == NULL) {

		return *this;
	}

	else {
		if (this == otherCell)
			return *this;
		if (m_cellType != otherCell->m_cellType) {
			this->m_cellType = otherCell->m_cellType;


		}

		if (m_line != otherCell->m_line) {
			this->m_line = otherCell->m_line;
		}
		if (m_column != otherCell->m_column) {
			this->m_column = otherCell->m_column;
		}
	}
	return *this;
}

/*Cell::Cell(std::shared_ptr<Cell>&& other) noexcept
{
	this->m_cellType = other->m_cellType;
	this->m_column= other->m_column;
	this->m_line = other->m_line;
	this->m_isFlagged = other->m_isFlagged;
	other = nullptr;

}
*/
/*Cell::Cell(std::shared_ptr<Cell>&& other) noexcept
{
	*this = std::move(other);
}
*/


bool Cell::operator!=(const Cell& otherCell)
{
	
	return m_line != otherCell.m_line && m_column != otherCell.m_column;
	
}

/*Cell& Cell::operator=(const std::shared_ptr<Cell>&& other)
{

	*this = std::move(other);
	return *this;

	
}
*/


void Cell::SetCellType(const CellType cellType)
{
	this->m_cellType = cellType;
}

Cell::CellType Cell::GetCellType() const
{
	return m_cellType;
}

void Cell::SetLine(const uint8_t line)
{
	m_line = line;
}

const uint8_t Cell::GetLine() const
{
	return m_line;
}

void Cell::SetColumn(const uint8_t column)
{
	m_column = column;
}

const uint8_t Cell::GetColumn() const
{
	return m_column;
}

void Cell::SetAsFlagged()
{
	m_isFlagged = true;
}

const bool Cell::CheckIfFlagged() const
{
	return m_isFlagged;
}

void Cell::SetAsUnflagged() {
	m_isFlagged = false;
}


