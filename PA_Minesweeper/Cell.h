#pragma once


#include <string>
#include <memory>
#include <SFML/Graphics.hpp>

class Cell
{


public:

	enum class CellType :uint8_t
	{

		Clear,
		Bomb,



	};

	Cell(const uint8_t, const uint8_t);

	
	Cell(const std::shared_ptr<Cell>&);





	Cell(const Cell&);
	Cell& operator=(const Cell&);

	
	Cell& operator=(const std::shared_ptr<Cell>&);

	

	bool operator !=(const Cell&);




	void SetCellType(const CellType cellType);
	CellType GetCellType() const;
	void SetLine(const uint8_t);
	const uint8_t GetLine() const;
	void SetColumn(const uint8_t);
	const uint8_t GetColumn() const;
	void SetAsFlagged();
	const bool CheckIfFlagged() const;
	void SetAsUnflagged();





private:
	CellType m_cellType;
	uint8_t m_line;
	uint8_t m_column;
	bool m_isFlagged;


};



