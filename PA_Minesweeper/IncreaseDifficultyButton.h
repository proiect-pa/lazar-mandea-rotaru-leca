#pragma once
#include "IButton.h"
class IncreaseDifficultyButton : public IButton
{
public:

	IncreaseDifficultyButton();
	
	void SetButtonCoordinates(const Difficulty currentGameDifficulty);
	VisualElement& GetVisualElement();
	const VisualElement& GetVisualElement() const;

private:
	VisualElement m_visualElement;



};

