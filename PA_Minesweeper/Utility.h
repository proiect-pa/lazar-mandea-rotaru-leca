#pragma once



	
template<typename E>
inline constexpr auto ToUnderlyingType(E enumElement)
	{
		return static_cast<typename std::underlying_type<E>::type>(enumElement);
	}