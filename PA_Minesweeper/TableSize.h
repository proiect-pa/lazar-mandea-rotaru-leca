#pragma once

#include <unordered_map>
#include "Difficulty.h"





class TableSize {

public:

	enum class TableHeight :uint8_t {

		BeginnerHeight = 8,
		IntermediateHeight = 16,
		ExpertHeight = 16


	};

	enum class TableWidth :uint8_t {

		BeginnerWidth = 8,
		IntermediateWidth = 16,
		ExpertWidth = 30


	};

	using sizeMap = std::unordered_map<Difficulty, std::pair<TableSize::TableHeight, TableSize::TableWidth>>;
	static const sizeMap difficultySizeMap;
	

	


private:
	


};


