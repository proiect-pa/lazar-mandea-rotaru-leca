#include "DecreaseDifficultyButton.h"





DecreaseDifficultyButton::DecreaseDifficultyButton()
{
	m_visualElement.LoadTexture("images/decreasedifficulty.png");
}

void DecreaseDifficultyButton::SetButtonCoordinates(const Difficulty currentGameDifficulty)
{

	


		switch (currentGameDifficulty) {
		case Difficulty::Beginner:

			
			m_visualElement.AssignCoordinates(250, 20);
			
			break;
		case Difficulty::Intermediate:

			m_visualElement.AssignCoordinates(450, 50);
			
			break;
		case Difficulty::Expert:

			m_visualElement.AssignCoordinates(650, 60);

			
			break;


		}
	

}

VisualElement& DecreaseDifficultyButton::GetVisualElement()
{
	return std::ref(m_visualElement);
}

const VisualElement& DecreaseDifficultyButton::GetVisualElement() const
{
	return m_visualElement;
}