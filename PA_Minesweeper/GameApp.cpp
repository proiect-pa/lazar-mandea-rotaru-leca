#include "GameApp.h"

#include <iostream>
#include <SFML/Graphics.hpp>
#include <thread>
#include <stack>
#include <vector>



const std::string filePath = "log.txt";
Logger& gameAppLogger = LoggerSingleton::getLoggerInstance(filePath);


GameApp::GameApp()
{


	gameAppLogger.Log("Class GameApp's constructor has been called !", Logger::Level::Info);

	this->m_minesweeperTable.FillTable();
	this->m_minesweeperTable.GenerateBombs();
	m_gameState = GameState::Ongoing;
	UpdateCellsToDiscoverNumber();
	m_timeCounter.SetCounterCoordinates(m_minesweeperTable.GetCurrentDifficulty());


	if (!CheckIfCustomMode()) {
		const auto [gameTableHeight, gameTableWidth] = TableSize::difficultySizeMap.find(m_minesweeperTable.GetCurrentDifficulty())->second;


		ResizeGridRepresentation(ToUnderlyingType(gameTableHeight), ToUnderlyingType(gameTableWidth));
	}

	else {
		const auto gameTableHeight = m_minesweeperTable.GetTableContainer().GetCustomHeight();
		const auto gameTableWidth = m_minesweeperTable.GetTableContainer().GetCustomWidth();

		ResizeGridRepresentation(gameTableHeight, gameTableWidth);
	}





	InitializeGridRepresentation();



}

void GameApp::Run()
{
	gameAppLogger.Log("Class GameApp's Run() method has been called !", Logger::Level::Info);

	sf::RenderWindow gameWindow(sf::VideoMode::getDesktopMode(), "Minesweeper");


	AdjustScreenSize(gameWindow);

	bool mouseDragged = false, leftClick = false, middleClick=false;
	std::stack<std::shared_ptr<Cell>> cellsToErase;

	

	Set middleClickSet, cellsTrail;


	sf::Texture tileTexture;
	sf::Sprite tileSprite;
	HelperSFML::LoadTileTexture(tileTexture, tileSprite);
	std::vector <std::shared_ptr<Cell>> cellNeighbors;


	while (gameWindow.isOpen()) {



		sf::Event event;


		uint16_t mouseCoordX, mouseCoordY;

		while (gameWindow.pollEvent(event)) {



			


			if (event.type == sf::Event::Closed)
			{

				gameWindow.close();
			}

			if (!GameOver()) {
				AdjustMouseCoordinates(mouseCoordX, mouseCoordY, gameWindow);
				//gameAppLogger.Log("Mouse coordinates have been adjusted.", Logger::Level::Info);
				if (event.type == sf::Event::MouseButtonPressed){

					if (event.key.code == sf::Mouse::Left) {

						mouseDragged = true;
						leftClick = true;

						m_smileyFaceButton.SetClickReaction();
						gameAppLogger.Log("The emote's reaction has been set to 'amazed'.", Logger::Level::Info);


						gameAppLogger.Log("The CheckClickOnCell() method has been called.", Logger::Level::Info);
					}

					else if (event.key.code == sf::Mouse::Middle) {

						mouseDragged = true;
						middleClick = true;

						m_smileyFaceButton.SetClickReaction();
						gameAppLogger.Log("The emote's reaction has been set to 'amazed'.", Logger::Level::Info);

					}


				}

				if (leftClick) {
					for (const auto& setElement : cellsTrail) {
						if (ValidMouseCoordinates(mouseCoordX, mouseCoordY) && mouseCoordX != setElement->GetLine() || mouseCoordY != setElement->GetColumn()) {
							cellsToErase.push(setElement);

						}
					}
					while (!cellsToErase.empty()) {

						m_gridRepresentation.at(cellsToErase.top()->GetLine()).at(cellsToErase.top()->GetColumn()) = 10u;
						cellsTrail.erase(cellsToErase.top());
						cellsToErase.pop();

					}
				}

				else if (middleClick) {
					
					cellNeighbors.clear();
					CellNeighbors(cellNeighbors, m_minesweeperTable.GetTableContainer(), mouseCoordX,mouseCoordY);
					
					for (const auto& setElement : middleClickSet) {
						
							if ( m_gridRepresentation.at(setElement->GetLine()).at(setElement->GetColumn()) == 0u) {

								cellsToErase.push(setElement);
							}

						
					}

					while (!cellsToErase.empty()) {

						m_gridRepresentation.at(cellsToErase.top()->GetLine()).at(cellsToErase.top()->GetColumn()) = 10u;
						middleClickSet.erase(cellsToErase.top());
						cellsToErase.pop();
					}
					

				}


				if (mouseDragged) {
					if (leftClick) {
						if (ValidMouseCoordinates(mouseCoordX, mouseCoordY) && m_gridRepresentation.at(mouseCoordX).at(mouseCoordY) == 10u) {
							cellsTrail.emplace(m_minesweeperTable.GetTableContainer().GetElement(mouseCoordX, mouseCoordY));
							m_gridRepresentation.at(mouseCoordX).at(mouseCoordY) = 0u;

						}
					}

					else if (middleClick) {
						if (ValidMouseCoordinates(mouseCoordX, mouseCoordY) && m_gridRepresentation.at(mouseCoordX).at(mouseCoordY) == 10u) {
							middleClickSet.emplace(m_minesweeperTable.GetTableContainer().GetElement(mouseCoordX, mouseCoordY));
							m_gridRepresentation.at(mouseCoordX).at(mouseCoordY) = 0u;
							for (const auto& neighbor : cellNeighbors) {
								
								if (m_gridRepresentation.at(neighbor->GetLine()).at(neighbor->GetColumn()) == 10u) {
									middleClickSet.emplace(neighbor);

									m_gridRepresentation.at(neighbor->GetLine()).at(neighbor->GetColumn()) = 0u;
								}


							}

						}


					}
				}


				if (event.type == sf::Event::MouseButtonReleased) {

					mouseDragged = false;
					
					if (middleClick) {
						for (const auto& neighbor : middleClickSet) {

							
							if (m_gridRepresentation.at(neighbor->GetLine()).at(neighbor->GetColumn()) == 0u ) {
								m_gridRepresentation.at(neighbor->GetLine()).at(neighbor->GetColumn()) = 10u;
							}
							


						}


						
						middleClick = false;
						middleClickSet.clear();
					}


					
					if (leftClick && !middleClick) {
						if (!cellsTrail.empty() ) {
							m_gridRepresentation.at(cellsTrail.begin()->get()->GetLine()).at(cellsTrail.begin()->get()->GetColumn()) = 10u;
							cellsTrail.erase(cellsTrail.begin());
						}
						leftClick = false;
						cellsTrail.clear();
					}
					
					
					
					

					m_smileyFaceButton.SetHappyReaction();
					CheckClickOnCell(mouseCoordX, mouseCoordY, event);
					gameAppLogger.Log("The emote's reaction has been set to 'happy'.", Logger::Level::Info);



					if (GameOver()) {

						m_smileyFaceButton.SetLossReaction();
						gameAppLogger.Log("The current game has come to an end (loss). Therefore, the emote's reaction has become 'sad'.", Logger::Level::Info);
					}


				}
				if (GameWon()) {

					SetGameState(GameState::Win);
					m_smileyFaceButton.SetWinReaction();
					EndGameAudio();
					gameAppLogger.Log("The player has won the game. Therefore, the emote's reaction has been set to 'cool'.", Logger::Level::Info);


				}


			}

			if (event.type == sf::Event::MouseButtonPressed) {


				const auto& [coordinateX, coordinateY] = sf::Mouse::getPosition(gameWindow);
				CheckClickOnButton(coordinateX, coordinateY, event, gameWindow);
				gameAppLogger.Log("CheckClickOnButton() method has been called.", Logger::Level::Info);

			}





		}



		UpdateTimeCounter();

		gameWindow.clear(sf::Color::White);


		UpdateMinesweeperTiles(gameWindow, tileSprite);
		UpdateVisualElements(gameWindow);
		

		
		gameWindow.display();





	}







}

const MinesweeperTable& GameApp::GetMinesweeperTable() const
{
	return this->m_minesweeperTable;
}

const GameApp::GameState GameApp::GetCurrentGameState() const
{
	return m_gameState;
}

const bool GameApp::ValidMouseCoordinates(const uint16_t mouseCoordX, const uint16_t mouseCoordY)
{


	if (!CheckIfCustomMode()) {
		const auto [tableHeight, tableWidth] = m_minesweeperTable.GetHeightWidth();

		return mouseCoordX >= ToUnderlyingType(tableHeight) || mouseCoordY >= ToUnderlyingType(tableWidth) ? false : true;
	}

	else {
		const auto tableHeight = m_minesweeperTable.GetTableContainer().GetCustomHeight();
		const auto tableWidth = m_minesweeperTable.GetTableContainer().GetCustomWidth();
		return mouseCoordX >= tableHeight || mouseCoordY >= tableWidth ? false : true;
	}
}



const bool GameApp::UncoveredCell(const uint8_t line, const uint8_t column) const
{
	return m_gridRepresentation.at(line).at(column) == 10u || m_gridRepresentation.at(line).at(column) == 11u;
}

const bool GameApp::FlaggedCell(const uint8_t line, const uint8_t column) const
{
	return m_gridRepresentation.at(line).at(column) == 11u;
}



const bool GameApp::GameWon() const
{

	return m_cellsToDiscover == 0;
}

void GameApp::PlaceNumberOfBombsTexture(const uint16_t mouseCoordX, const uint16_t mouseCoordY)
{
	auto numberOfBombs = m_minesweeperTable.NearbyNoBombs(m_minesweeperTable.GetTableContainer().GetElement(mouseCoordX, mouseCoordY));
	if (numberOfBombs != 0) {
		m_gridRepresentation.at(mouseCoordX).at(mouseCoordY) = numberOfBombs;
		--m_cellsToDiscover;
	}

	else {

		gameAppLogger.Log("BFS function has been called.", Logger::Level::Info);
		const auto& currentCell = m_minesweeperTable.GetTableContainer().GetElement(mouseCoordX, mouseCoordY);
		
		BFS(m_minesweeperTable, currentCell, m_gridRepresentation, m_cellsToDiscover);
	}
}

void GameApp::IncreaseGameDifficulty()
{
	gameAppLogger.Log("The current game's difficulty has been increased.", Logger::Level::Info);

	m_minesweeperTable.IncreaseDifficultyLevel();

	if (CheckIfCustomMode())
		SetCustomMode(false);
	StartNewGame();

}

void GameApp::DecreaseGameDifficulty()
{
	gameAppLogger.Log("The current game's difficulty has been decreased.", Logger::Level::Info);
	m_minesweeperTable.DecreaseDifficultyLevel();

	if (CheckIfCustomMode())
		SetCustomMode(false);
	StartNewGame();

}

void GameApp::StartNewGame()
{

	gameAppLogger.Log("A new game has been started.", Logger::Level::Info);
	m_minesweeperTable.GetTableContainer().Clear();
	m_minesweeperTable.FillTable();
	m_minesweeperTable.GenerateBombs();
	if (!CheckIfCustomMode()) {
		const auto [tableHeight, tableWidth] = m_minesweeperTable.GetHeightWidth();
		ResizeGridRepresentation(ToUnderlyingType(tableHeight), ToUnderlyingType(tableWidth));
		InitializeGridRepresentation();
		m_flagCounter.UpdateFlagDefaultValue(m_minesweeperTable.GetCurrentDifficulty());
	}
	else {
		const auto tableHeight = m_minesweeperTable.GetTableContainer().GetCustomHeight();
		const auto tableWidth = m_minesweeperTable.GetTableContainer().GetCustomWidth();
		ResizeGridRepresentation(tableHeight, tableWidth);
		InitializeGridRepresentation();
		m_flagCounter.AssignCoordinates(1400, 35);
		m_timeCounter.AssignCoordinates(400, 35);
		m_flagCounter.SetCustomModeValue(m_minesweeperTable.GetTableContainer().GetCustomNoBombs());

	}

	m_smileyFaceButton.SetHappyReaction();
	SetGameState(GameState::Ongoing);
	UpdateCellsToDiscoverNumber();
	m_timeCounter.ResetClock();
	UpdateTimeCounter();

	
	
}

void GameApp::UpdateMinesweeperTiles(sf::RenderWindow& gameWindow, sf::Sprite& tileSprite) 
{

	
		for (const auto& element : m_minesweeperTable.GetTableContainer().GetContainer())
		{

			tileSprite.setTextureRect(sf::IntRect(m_gridRepresentation.at(element->GetLine()).at(element->GetColumn()) * kTileWidth, 0, kTileWidth, kTileWidth));
			if (!CheckIfCustomMode()) {
				tileSprite.setPosition(HelperSFML::GetColumnOffset(m_minesweeperTable.GetCurrentDifficulty()) + (element->GetColumn() * kTileWidth),
					HelperSFML::GetLineOffset(m_minesweeperTable.GetCurrentDifficulty()) +
					(element->GetLine()) * kTileWidth);
				
			}

			else {
				const auto [customLineOffset, customColumnOffset] = HelperSFML::GetCustomOffsets(m_minesweeperTable.GetTableContainer().GetCustomHeight(), 
					m_minesweeperTable.GetTableContainer().GetCustomWidth());
				tileSprite.setPosition(customColumnOffset + (element->GetColumn() * kTileWidth), customLineOffset +
					(element->GetLine()) * kTileWidth);


			}
			gameWindow.draw(tileSprite);

		}
	








}





void GameApp::AdjustMouseCoordinates(uint16_t& mouseCoordX, uint16_t& mouseCoordY, const sf::RenderWindow& gameWindow)
{
	auto mousePosition = sf::Mouse::getPosition(gameWindow);

	if (!CheckIfCustomMode()) {
		mouseCoordX = (mousePosition.y - HelperSFML::GetLineOffset(m_minesweeperTable.GetCurrentDifficulty()));
		mouseCoordY = (mousePosition.x - HelperSFML::GetColumnOffset(m_minesweeperTable.GetCurrentDifficulty()));
	}

	else {
		//mouseCoordX = (mousePosition.y - HelperSFML::GetLineOffset(m_minesweeperTable.GetCurrentDifficulty()));
		//mouseCoordY = (mousePosition.x - HelperSFML::GetColumnOffset(m_minesweeperTable.GetCurrentDifficulty()));
		
		const auto& customMouseCoordinates = HelperSFML::GetCustomOffsets(m_minesweeperTable.GetTableContainer().GetCustomHeight(),m_minesweeperTable.GetTableContainer().GetCustomWidth());
		mouseCoordX = mousePosition.y - customMouseCoordinates.first;
		mouseCoordY = mousePosition.x - customMouseCoordinates.second;

	}

	mouseCoordX /= kTileWidth;
	mouseCoordY /= kTileWidth;




}

void GameApp::UncoverAllBombs()
{

	gameAppLogger.Log("All the bombs on the table have been uncovered because the player had lost.", Logger::Level::Info);


	const auto& tableContainer = m_minesweeperTable.GetTableContainer().GetContainer();

	for (const auto& element : tableContainer) {

		if (element->GetCellType() == Cell::CellType::Bomb) {

			m_gridRepresentation.at(element->GetLine()).at(element->GetColumn()) = 9;
		}

	}
}

void GameApp::UpdateButtons()
{
	if (!CheckIfCustomMode()) {
		m_increaseDifficultyButton.SetButtonCoordinates(m_minesweeperTable.GetCurrentDifficulty());
		m_decreaseDifficultyButton.SetButtonCoordinates(m_minesweeperTable.GetCurrentDifficulty());
		m_smileyFaceButton.SetButtonCoordinates(m_minesweeperTable.GetCurrentDifficulty());
		m_customDifficultyButton.SetButtonCoordinates(m_minesweeperTable.GetCurrentDifficulty());
	}

	else {
		m_increaseDifficultyButton.GetVisualElement().AssignCoordinates(600, 30);
		m_decreaseDifficultyButton.GetVisualElement().AssignCoordinates(1200, 30);
		m_smileyFaceButton.GetVisualElement().AssignCoordinates(900, 30);
		m_customDifficultyButton.GetVisualElement().AssignCoordinates(900, 950);

	}
}

void GameApp::UpdateCounters()
{
	if (!CheckIfCustomMode()) {
	m_timeCounter.SetCounterCoordinates(m_minesweeperTable.GetCurrentDifficulty());
	m_flagCounter.SetCounterCoordinates(m_minesweeperTable.GetCurrentDifficulty());

	}
	else{
		m_timeCounter.AssignCoordinates(400,35);
		m_flagCounter.AssignCoordinates(1400, 35);
		m_flagCounter.SetCustomModeValue(m_minesweeperTable.GetTableContainer().GetCustomNoBombs());
	}


}

void GameApp::CheckClickOnCell(const uint16_t mouseCoordX, const uint16_t mouseCoordY, const sf::Event& event)
{
	
	if (ValidMouseCoordinates(mouseCoordX, mouseCoordY) && UncoveredCell(mouseCoordX, mouseCoordY)) {
		
		if (event.key.code == sf::Mouse::Left) {
			

			if (!m_minesweeperTable.GetTableContainer().GetElement(mouseCoordX, mouseCoordY)->CheckIfFlagged()) {

				if (!isBomb(m_minesweeperTable.GetTableContainer(), m_minesweeperTable.GetTableContainer().GetElement(mouseCoordX, mouseCoordY))
					)
				{
					gameAppLogger.Log("The selected cell has been uncovered", Logger::Level::Info);
					PlaceNumberOfBombsTexture(mouseCoordX, mouseCoordY);

				}
				else {

					gameAppLogger.Log("The game has come to an end (loss) because the user clicked on a bomb. Therefore, the emote's reaction has been set to 'sad'", Logger::Level::Info);
					m_smileyFaceButton.SetLossReaction();
					SetGameState(GameState::Loss);
					UncoverAllBombs();
					m_gridRepresentation.at(mouseCoordX).at(mouseCoordY) = 12;
					EndGameAudio();



				}
			}
		}



		if (event.key.code == sf::Mouse::Right) {

			if (!FlaggedCell(mouseCoordX, mouseCoordY)) {

				gameAppLogger.Log("A cell has been flagged.", Logger::Level::Info);


				m_minesweeperTable.GetTableContainer().GetElement(mouseCoordX, mouseCoordY)->SetAsFlagged();
				m_gridRepresentation.at(mouseCoordX).at(mouseCoordY) = 11u;
				(m_flagCounter.CheckIfNegativeSign()) ? m_flagCounter.TickUp() : m_flagCounter.TickDown();


			}

			else {
				m_gridRepresentation.at(mouseCoordX).at(mouseCoordY) = 10u;
				//m_flagCounter.TickUp();
				m_minesweeperTable.GetTableContainer().GetElement(mouseCoordX, mouseCoordY)->SetAsUnflagged();
				(m_flagCounter.CheckIfNegativeSign()) ? m_flagCounter.TickDown() : m_flagCounter.TickUp();

				gameAppLogger.Log("A cell has been unflagged.", Logger::Level::Info);
			}


		}






	}
}

void GameApp::CheckClickOnButton(const int32_t mouseCoordX, const int32_t mouseCoordY, const sf::Event& event, sf::RenderWindow& gameWindow)
{
	if (event.key.code == sf::Mouse::Left) {

		if (m_increaseDifficultyButton.GetVisualElement().ContainsCoordinates(mouseCoordX, mouseCoordY, HelperSFML::kDifficultyButtonHeight, HelperSFML::kDifficultyButtonWidth)) {

			IncreaseGameDifficulty();
			AdjustScreenSize(gameWindow);
		}
		else if (m_decreaseDifficultyButton.GetVisualElement().ContainsCoordinates(mouseCoordX, mouseCoordY, HelperSFML::kDifficultyButtonHeight, HelperSFML::kDifficultyButtonWidth)) {




			DecreaseGameDifficulty();
			AdjustScreenSize(gameWindow);
		}
		else if (m_smileyFaceButton.GetVisualElement().ContainsCoordinates(mouseCoordX, mouseCoordY, kTileWidth, kTileWidth)) {

			gameAppLogger.Log("Smiley face button has been pressed.", Logger::Level::Info);

			StartNewGame();

		}

		else if (m_customDifficultyButton.GetVisualElement().ContainsCoordinates(mouseCoordX, mouseCoordY, HelperSFML::kCustomDifficultyButtonHeight, HelperSFML::kCustomDifficultyButtonWidth)) {
			gameAppLogger.Log("Custom mode button has been pressed.", Logger::Level::Info);
			SetCustomMode(true);
			CustomValuesInput();
			StartNewGame();

			AdjustScreenSize(gameWindow);
			const auto gameTableHeight = m_minesweeperTable.GetTableContainer().GetCustomHeight();
			const auto gameTableWidth = m_minesweeperTable.GetTableContainer().GetCustomWidth();

			ResizeGridRepresentation(gameTableHeight, gameTableWidth);
			InitializeGridRepresentation();



		}

	}
}

void GameApp::AdjustScreenSize(sf::RenderWindow& gameWindow)
{

	if (!CheckIfCustomMode()) {
		const auto& currentDifficultyLevel = m_minesweeperTable.GetCurrentDifficulty();
		switch (currentDifficultyLevel) {
		case Difficulty::Beginner:
			gameWindow.create(sf::VideoMode(400, 400), "Minesweeper");
			break;
		case Difficulty::Intermediate:
			gameWindow.create(sf::VideoMode(800, 800), "Minesweeper");
			break;
		case Difficulty::Expert:
			gameWindow.create(sf::VideoMode(1200, 800), "Minesweeper");
			break;

		}
	}

	else {
		gameWindow.create(sf::VideoMode(1920, 1080), "Minesweeper");

	}

	gameAppLogger.Log("Screen's size has been adjusted according to the current difficulty.", Logger::Level::Info);

	UpdateButtons();
	UpdateCounters();

}

void GameApp::InitializeGridRepresentation()
{


	for (auto& row : m_gridRepresentation) {
		for (auto& column : row) {
			column = 10u;
		}

	}

	gameAppLogger.Log("Grid representation container has been initialised.", Logger::Level::Info);
}

void GameApp::ResizeGridRepresentation(const uint8_t tableHeight, const uint8_t tableWidth) {




	m_gridRepresentation.resize(tableHeight);

	for (auto& row : m_gridRepresentation) {

		row.resize(tableWidth);

	}
	gameAppLogger.Log("Grid representation container has been resized.", Logger::Level::Info);
}

void GameApp::UpdateCellsToDiscoverNumber()
{

	if (!CheckIfCustomMode()) {
		const auto& currentGameDifficulty = m_minesweeperTable.GetCurrentDifficulty();
		switch (currentGameDifficulty) {
		case Difficulty::Beginner:
			m_cellsToDiscover = 54;
			break;
		case Difficulty::Intermediate:
			m_cellsToDiscover = 216;
			break;
		case Difficulty::Expert:
			m_cellsToDiscover = 381;

		}
	}

	else {

		m_cellsToDiscover = (m_minesweeperTable.GetTableContainer().GetCustomHeight() * m_minesweeperTable.GetTableContainer().GetCustomWidth()) -
			m_minesweeperTable.GetTableContainer().GetCustomNoBombs();
	}



}



void GameApp::SetGameState(const GameState gameState)
{
	m_gameState = gameState;
}

const bool GameApp::GameOver() const
{
	return m_gameState == GameState::Loss || m_gameState == GameState::Win;
}

void GameApp::EndGameAudio()
{


	switch (m_gameState) {


	case GameState::Loss:


		m_endGameSound.openFromFile("audio/bomb.wav");
		m_endGameSound.setVolume(10);



		break;
	case GameState::Win:
		m_endGameSound.openFromFile("audio/triumph.wav");
		m_endGameSound.setVolume(10);

		break;
	}

	m_endGameSound.play();

	gameAppLogger.Log("End game audio has been played.", Logger::Level::Info);

}

void GameApp::ThreadIncrementDigit()
{


	if (m_timeCounter.GetClock().getElapsedTime().asSeconds() >= 1) {


		gameAppLogger.Log("Time counter has been updated (increased).", Logger::Level::Info);

		m_timeCounter.TickUp();


		m_timeCounter.StartClock();
	}

}

void GameApp::UpdateTimeCounter()
{
	if (GameStarted() && !GameOver()) {

		if (!m_timeCounter.TimeIsUp())
		{


			std::thread thread(std::bind(&GameApp::ThreadIncrementDigit, this));
			thread.join();
		}
		else {
			gameAppLogger.Log("Game has been stopped because the time ran out.", Logger::Level::Info);
			SetGameState(GameState::Loss);
			m_smileyFaceButton.SetLossReaction();
		}

	}

}

const bool GameApp::GameStarted()
{
	for (const auto& row : m_gridRepresentation) {

		for (const auto& column : row) {
			if (column != 10)
				return true;

		}
	}
	return false;
}

void GameApp::SetCustomMode(const bool customDifficultyMode)
{
	m_minesweeperTable.GetTableContainer().SetCustomMode(customDifficultyMode);
}

const bool GameApp::CheckIfCustomMode()
{
	return m_minesweeperTable.GetTableContainer().CheckIfCustomMode();
}

void GameApp::UpdateVisualElements(sf::RenderWindow& gameWindow)
{
	m_increaseDifficultyButton.GetVisualElement().DrawElement(gameWindow);
	m_decreaseDifficultyButton.GetVisualElement().DrawElement(gameWindow);
	m_smileyFaceButton.GetVisualElement().DrawElement(gameWindow);
	m_timeCounter.DrawDigits(gameWindow);
	m_flagCounter.DrawDigits(gameWindow);
	m_customDifficultyButton.GetVisualElement().DrawElement(gameWindow);


}

void GameApp::CustomValuesInput()
{


	uint16_t customHeight, customWidth, customBombs;

	while (true) {
		std::cout << "Custom height: (between 1 and 27) \n ";
		std::cin >> customHeight;
		if (!(customHeight <= NULL || customHeight > HelperSFML::kCustomDifficultyMaxHeight)) {

			break;
		}

		else {

			gameAppLogger.Log("The input for custom mode (height) is out of bounds !", Logger::Level::Warning);
		}
	}

	while (true) {
		std::cout << "Custom width: (between 1 and 54) \n ";
		std::cin >> customWidth;
		if (!(customWidth <= NULL || customWidth > HelperSFML::kCustomDifficultyMaxWidth)) {
			break;
		}

		else {

			gameAppLogger.Log("The input for custom mode (width) is out of bounds !", Logger::Level::Warning);
		}
	}

	while (true) {
		std::cout << "Custom number of bombs: (between 1 and " << (customHeight * customWidth) - 1 << ")\n";
		std::cin >> customBombs;
		if (!(customBombs < NULL || customBombs >((customHeight * customWidth) - 1))) {

			break;
		}


		else {

			gameAppLogger.Log("The input for custom mode (number of bombs) is out of bounds !", Logger::Level::Warning);
		}
	}


	m_minesweeperTable.GetTableContainer().SetCustomModeValues(customHeight, customWidth, customBombs);

	gameAppLogger.Log("Input for custom mode has been succesfully stored !", Logger::Level::Info);


	std::cout << "\n\n\n";
}








