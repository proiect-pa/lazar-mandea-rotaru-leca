#pragma once
#include "IButton.h"

class DecreaseDifficultyButton : public IButton
{  public:

	
	DecreaseDifficultyButton();
	
   void SetButtonCoordinates(const Difficulty currentGameDifficulty);
   VisualElement& GetVisualElement();
   const VisualElement& GetVisualElement() const;

private:
	VisualElement m_visualElement;

};

