#pragma once
#include <SFML/Graphics.hpp>
#include <array>
#include "CounterDigit.h"
#include "Difficulty.h"

class ICounter {
public:
	ICounter() = default;

	virtual void SetCounterCoordinates(const Difficulty)=0;
	virtual const bool IncreaseIndex(const uint8_t) = 0;
	virtual void TickUp() = 0;
	virtual void DrawDigits(sf::RenderWindow&) = 0;
	virtual ~ICounter() = default;
	virtual void AssignCoordinates(const float, const float) = 0;






};