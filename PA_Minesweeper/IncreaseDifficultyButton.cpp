#include "IncreaseDifficultyButton.h"

IncreaseDifficultyButton::IncreaseDifficultyButton()
{
	m_visualElement.LoadTexture("images/increasedifficulty.png");
}

void IncreaseDifficultyButton::SetButtonCoordinates(const Difficulty currentGameDifficulty) {

	
	switch (currentGameDifficulty) {
	case Difficulty::Beginner:
		
		
		m_visualElement.AssignCoordinates(120, 20);
		
		break;
	case Difficulty::Intermediate:
		
		
		m_visualElement.AssignCoordinates(270, 50);
	
		break;
	case Difficulty::Expert:
		
		 
		m_visualElement.AssignCoordinates(450, 60);
		 
	
		break;


	}
}

VisualElement& IncreaseDifficultyButton::GetVisualElement()
{
	return std::ref(m_visualElement);
}

const VisualElement& IncreaseDifficultyButton::GetVisualElement() const
{
	return m_visualElement;
}
