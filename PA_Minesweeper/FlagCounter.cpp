#include "FlagCounter.h"

FlagCounter::FlagCounter()
{
	UpdateFlagDefaultValue(Difficulty::Beginner);
	
}

void FlagCounter::SetCounterCoordinates(const Difficulty currentGameDifficulty)
{
	

		switch (currentGameDifficulty) {


		case Difficulty::Beginner:
			m_digits.at(0).GetVisualElement().AssignCoordinates(335, 30);
			m_digits.at(1).GetVisualElement().AssignCoordinates(347, 30);
			m_digits.at(2).GetVisualElement().AssignCoordinates(359, 30);
			m_digits.at(3).GetVisualElement().AssignCoordinates(371, 30);
			break;
		case Difficulty::Intermediate:
			m_digits.at(0).GetVisualElement().AssignCoordinates(650, 60);
			m_digits.at(1).GetVisualElement().AssignCoordinates(662, 60);
			m_digits.at(2).GetVisualElement().AssignCoordinates(674, 60);
			m_digits.at(3).GetVisualElement().AssignCoordinates(686, 60);
			break;
		case Difficulty::Expert:
			m_digits.at(0).GetVisualElement().AssignCoordinates(1030, 70);
			m_digits.at(1).GetVisualElement().AssignCoordinates(1042, 70);
			m_digits.at(2).GetVisualElement().AssignCoordinates(1054, 70);
			m_digits.at(3).GetVisualElement().AssignCoordinates(1066, 70);
			break;
		}

	
}

const bool FlagCounter::IncreaseIndex(const uint8_t currentIndex)
{
	


	if (m_digits.at(currentIndex).HasMaxValue())
	{
		m_digits.at(currentIndex).SetDigitIndexNull();
		m_digits.at(currentIndex).UpdateDigit();
		return false;
	}

	else {

		m_digits.at(currentIndex).IncreaseDigitIndex();
		m_digits.at(currentIndex).UpdateDigit();
		return true;
	}
}

const bool FlagCounter::DecreaseIndex(const uint8_t currentIndex)
{
	if (m_digits.at(currentIndex).HasMinValue())
	{
		m_digits.at(currentIndex).SetDigitIndex(9);
		m_digits.at(currentIndex).UpdateDigit();
		return false;
	}

	else {

		m_digits.at(currentIndex).DecreaseDigitIndex();
		m_digits.at(currentIndex).UpdateDigit();
		return true;
	}
}

void FlagCounter::TickUp()
{
	if (CheckIfNegativeSign()) {

		if (m_digits.at(1).GetDigitIndex() == 10 && m_digits.at(3).GetDigitIndex() == 9 && m_digits.at(2).GetDigitIndex() == 9) {
			m_digits.at(0).SetDigitIndex(10);
			m_digits.at(1).SetDigitIndex(1);
			m_digits.at(2).SetDigitIndex(0);
			m_digits.at(3).SetDigitIndex(0);
			return;
		}
		
		
		if (m_digits.at(2).GetDigitIndex() == 10 && m_digits.at(3).GetDigitIndex() == 9)
		{
			m_digits.at(1).SetDigitIndex(10);
			m_digits.at(2).SetDigitIndex(1);
			m_digits.at(3).SetDigitIndex(0);
			return;
		}
		else if (m_digits.at(3).GetDigitIndex() == 9 && m_digits.at(1).GetDigitIndex() == 10) {

			m_digits.at(2).SetDigitIndex(m_digits.at(2).GetDigitIndex() + 1);
			m_digits.at(3).SetDigitIndex(0);
			return;
		}
		
		




	}
	if (IncreaseIndex(3))
		return;
	if (IncreaseIndex(2))
		return;
	if(IncreaseIndex(1))
		return;
	IncreaseIndex(0);
	
}

void FlagCounter::TickDown()
{

	if (CheckIfNegativeSign()) {


		if (m_digits.at(1).GetDigitIndex() == 1 && m_digits.at(2).GetDigitIndex() == 0 && m_digits.at(3).GetDigitIndex() == 0) {

			m_digits.at(0).SetDigitIndex(0);
			m_digits.at(1).SetDigitIndex(10);
			m_digits.at(2).SetDigitIndex(9);
			m_digits.at(3).SetDigitIndex(9);

			return;
		}



		if (m_digits.at(1).GetDigitIndex() == 10 && m_digits.at(2).GetDigitIndex() == 1 && m_digits.at(3).GetDigitIndex()==0) {

			m_digits.at(1).SetDigitIndex(0);
			m_digits.at(2).SetDigitIndex(10);
			m_digits.at(3).SetDigitIndex(9);

			return;
		}


		


	}

	if (ZeroFlagCount())
	{
		
		if (!CheckIfNegativeSign()) {
			m_digits.at(2).SetDigitIndex(10);
			m_digits.at(3).SetDigitIndex(1);
			m_digits.at(2).UpdateDigit();
			m_digits.at(3).UpdateDigit();
			SetNegativeSign(1);
			return;
		}
		else {
			m_digits.at(1).SetDigitIndex(0);
			m_digits.at(2).SetDigitIndex(0);
			m_digits.at(3).SetDigitIndex(0);
			SetNegativeSign(0);
			return;
		}

		
		

		
	}
	if (DecreaseIndex(3))
		return;
	if (DecreaseIndex(2))
		return;
	if (DecreaseIndex(1))
		return;
	DecreaseIndex(0);
	
}

const bool FlagCounter::ZeroFlagCount() const
{
	return m_digits.at(0).GetDigitIndex() == 0 && m_digits.at(1).GetDigitIndex() == 0 && m_digits.at(2).GetDigitIndex() == 0 && m_digits.at(3).GetDigitIndex() == 0 ||
		m_digits.at(2).GetDigitIndex()==10 && m_digits.at(3).GetDigitIndex()==1;
}

void FlagCounter::DrawDigits(sf::RenderWindow& gameWindow)
{
	m_digits.at(3).UpdateDigit();
	m_digits.at(2).UpdateDigit();
	m_digits.at(1).UpdateDigit();
	m_digits.at(0).UpdateDigit();
	m_digits.at(3).GetVisualElement().DrawElement(gameWindow);
	m_digits.at(2).GetVisualElement().DrawElement(gameWindow);
	m_digits.at(1).GetVisualElement().DrawElement(gameWindow);
	m_digits.at(0).GetVisualElement().DrawElement(gameWindow);

}

void FlagCounter::UpdateFlagDefaultValue(const Difficulty currentGameDifficulty)
{
	switch (currentGameDifficulty) {

	case Difficulty::Beginner:
		m_digits.at(0).SetDigitIndex(0);
		m_digits.at(1).SetDigitIndex(0);
		m_digits.at(2).SetDigitIndex(1);
		m_digits.at(3).SetDigitIndex(0);
		break;
	case Difficulty::Intermediate:
		m_digits.at(0).SetDigitIndex(0);
		m_digits.at(1).SetDigitIndex(0);
		m_digits.at(2).SetDigitIndex(4);
		m_digits.at(3).SetDigitIndex(0);
		break;
	case Difficulty::Expert:
		m_digits.at(0).SetDigitIndex(0);
		m_digits.at(1).SetDigitIndex(0);
		m_digits.at(2).SetDigitIndex(9);
		m_digits.at(3).SetDigitIndex(9);
		break;
	}
	SetNegativeSign(0);
}

void FlagCounter::SetNegativeSign( bool negativeSign)
{
	m_negativeSign = negativeSign;
}

 const bool FlagCounter::CheckIfNegativeSign()
{
	return m_negativeSign;
}

 void FlagCounter::SetCustomModeValue(const uint16_t customNoBombs)
 {
	 m_digits.at(0).SetDigitIndex(customNoBombs / 1000);
	 m_digits.at(1).SetDigitIndex((customNoBombs / 100)%10);
	 m_digits.at(2).SetDigitIndex((customNoBombs / 10)%10);
	 m_digits.at(3).SetDigitIndex(customNoBombs % 10);

 }

 void FlagCounter::AssignCoordinates(const float firstDigitCoordX, const float firstDigitCoordY)
 {
	 m_digits.at(0).GetVisualElement().AssignCoordinates(firstDigitCoordX, firstDigitCoordY);
	 m_digits.at(1).GetVisualElement().AssignCoordinates(firstDigitCoordX + HelperSFML::kCounterDigitOffset, firstDigitCoordY);
	 m_digits.at(2).GetVisualElement().AssignCoordinates(firstDigitCoordX + 2* HelperSFML::kCounterDigitOffset, firstDigitCoordY);
	 m_digits.at(3).GetVisualElement().AssignCoordinates(firstDigitCoordX + 3* HelperSFML::kCounterDigitOffset, firstDigitCoordY);
 }
