#pragma once
#include "VisualElement.h"
#include <string>

class CounterDigit 
{
public:

	CounterDigit();
	VisualElement& GetVisualElement();
	const VisualElement& GetVisualElement() const;
	const uint8_t GetDigitIndex() const;
	void IncreaseDigitIndex();
	void SetDigitIndexNull();
	const bool HasMaxValue();
	const bool HasMinValue();
	void DecreaseDigitIndex();
	void UpdateDigit();
	void SetDigitIndex(const uint8_t);

private:
	VisualElement m_visualElement;
	uint8_t m_digitIndex;
	const float kDigitWidth = 12.818f;
	const uint8_t kDigitHeight = 22;

	
};

