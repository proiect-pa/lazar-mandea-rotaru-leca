#pragma once
#include "ICounter.h"
#include "HelperSFML.h"

class FlagCounter : public ICounter
{
public:
	FlagCounter();
	 void SetCounterCoordinates(const Difficulty) ;
	 const bool IncreaseIndex(const uint8_t);
	 const bool DecreaseIndex(const uint8_t);
	 void TickUp();
	 void TickDown();
	 const bool ZeroFlagCount() const;
	 void DrawDigits(sf::RenderWindow&);
	 void UpdateFlagDefaultValue(const Difficulty);
	 void SetNegativeSign(bool);
	 const bool CheckIfNegativeSign();
	 void SetCustomModeValue(const uint16_t);
	 void AssignCoordinates(const float, const float);

private:
	bool m_negativeSign;
	std::array<CounterDigit, 4> m_digits;
};

