#include "CustomDifficultyButton.h"





CustomDifficultyButton::CustomDifficultyButton()
{
	m_visualElement.LoadTexture("images/customdifficulty.jpg");
}

void CustomDifficultyButton::SetButtonCoordinates(const Difficulty currentGameDifficulty)
{
	switch (currentGameDifficulty) {

	case Difficulty::Beginner:
		m_visualElement.AssignCoordinates(130, 330);
		break;
	case Difficulty::Intermediate:
		m_visualElement.AssignCoordinates(330, 680);
		break;
	case Difficulty::Expert:
		m_visualElement.AssignCoordinates(530, 680);
		break;
	}
}
VisualElement& CustomDifficultyButton::GetVisualElement()
{
	return std::ref(m_visualElement);
}

const VisualElement& CustomDifficultyButton::GetVisualElement() const
{
	return m_visualElement;
}