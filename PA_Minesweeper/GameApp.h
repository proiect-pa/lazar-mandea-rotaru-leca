#pragma once


#include "MinesweeperTable.h"
#include "HelperSFML.h"
#include <SFML/Graphics.hpp>
#include <array>
#include "IncreaseDifficultyButton.h"
#include "DecreaseDifficultyButton.h"
#include "SmileyFaceButton.h"
#include "TimeCounter.h"
#include <SFML/Audio.hpp>
#include "FlagCounter.h"
#include "CustomDifficultyButton.h"
#include "Utility.h"
#include "CellHash.h"
#include "EqualFunctionCell.h"



class GameApp
{
public:


	enum class GameState : uint8_t
	{
		Win,
		Loss,
		Ongoing


	};





public:
	GameApp();
	void Run();
	const MinesweeperTable& GetMinesweeperTable() const;
	const bool ValidMouseCoordinates(const uint16_t, const uint16_t);

	const bool UncoveredCell(const uint8_t, const uint8_t) const;
	const bool FlaggedCell(const uint8_t, const uint8_t) const;

	const bool GameWon() const;
	void PlaceNumberOfBombsTexture(const uint16_t, const uint16_t);
	void IncreaseGameDifficulty();
	void DecreaseGameDifficulty();
	void StartNewGame();
	void UpdateMinesweeperTiles(sf::RenderWindow&, sf::Sprite&);
	void AdjustMouseCoordinates(uint16_t& mouseCoordX, uint16_t& mouseCoordY, const sf::RenderWindow& gameWindow);
	void UncoverAllBombs();
	void UpdateButtons();
	void UpdateCounters();
	void CheckClickOnCell(const uint16_t, const uint16_t, const sf::Event&);
	void CheckClickOnButton(const int32_t mouseCoordX, const int32_t mouseCoordY, const sf::Event&, sf::RenderWindow&);

	void AdjustScreenSize(sf::RenderWindow&);

	void InitializeGridRepresentation();
	void ResizeGridRepresentation(const uint8_t, const uint8_t);
	void UpdateCellsToDiscoverNumber();
	



	const GameState GetCurrentGameState() const;

	void SetGameState(const GameState);
	const bool GameOver() const;
	void EndGameAudio();
	void ThreadIncrementDigit();
	void UpdateTimeCounter();
	const bool GameStarted();
	void SetCustomMode(const bool);
	const bool CheckIfCustomMode();
	void UpdateVisualElements(sf::RenderWindow&);


	void CustomValuesInput();





private:

	using Set = std::unordered_set <std::shared_ptr<Cell>, CellHash, EqualFunctionCell>;
	MinesweeperTable m_minesweeperTable;
	std::vector<std::vector<uint8_t>> m_gridRepresentation;
	static const uint8_t kTileWidth = 32u;
	GameState m_gameState;
	IncreaseDifficultyButton m_increaseDifficultyButton;
	DecreaseDifficultyButton m_decreaseDifficultyButton;
	SmileyFaceButton m_smileyFaceButton;
	TimeCounter m_timeCounter;
	FlagCounter m_flagCounter;
	CustomDifficultyButton m_customDifficultyButton;
	uint16_t m_cellsToDiscover;
	


	sf::Music m_endGameSound;

};

