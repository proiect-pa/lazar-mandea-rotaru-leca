#include "TimeCounter.h"



void TimeCounter::TickUp()
{
	
	

	if(IncreaseIndex(2))
		return ;
	if (IncreaseIndex(1))
		return ;
	IncreaseIndex(0);
		
	

}



void TimeCounter::SetCounterCoordinates(const Difficulty currentGameDifficulty)
{

	switch (currentGameDifficulty) {


	case Difficulty::Beginner:
		m_digits.at(0).GetVisualElement().AssignCoordinates(35,30);
		m_digits.at(1).GetVisualElement().AssignCoordinates(47,30);
		m_digits.at(2).GetVisualElement().AssignCoordinates(59,30);
		break;
	case Difficulty::Intermediate:
		m_digits.at(0).GetVisualElement().AssignCoordinates(70, 60);
		m_digits.at(1).GetVisualElement().AssignCoordinates(82, 60);
		m_digits.at(2).GetVisualElement().AssignCoordinates(94, 60);
		break;
	case Difficulty::Expert:
		m_digits.at(0).GetVisualElement().AssignCoordinates(110, 70);
		m_digits.at(1).GetVisualElement().AssignCoordinates(122, 70);
		m_digits.at(2).GetVisualElement().AssignCoordinates(134, 70);
		break;
	}

}

void TimeCounter::StartClock()
{
	m_clock.restart();
}

void TimeCounter::ResetClock()
{
	for (auto& digit : m_digits)
		digit.SetDigitIndexNull();
	

}

const bool TimeCounter::TimeIsUp()
{
	return m_digits.at(0).HasMaxValue() && m_digits.at(1).HasMaxValue() && m_digits.at(2).HasMaxValue();
}

const bool TimeCounter::IncreaseIndex(const uint8_t currentIndex)
{

	if (m_digits.at(currentIndex).HasMaxValue())
	{
		m_digits.at(currentIndex).SetDigitIndexNull();
		m_digits.at(currentIndex).UpdateDigit();
		return false;
	}

	else {

		m_digits.at(currentIndex).IncreaseDigitIndex();
		m_digits.at(currentIndex).UpdateDigit();
		return true;
	}
}

sf::Clock& TimeCounter::GetClock()
{
	return std::ref(m_clock);
}

const sf::Clock& TimeCounter::GetClock() const
{
	return m_clock;
}

void TimeCounter::DrawDigits(sf::RenderWindow& gameWindow)
{
	m_digits.at(2).UpdateDigit();
	m_digits.at(1).UpdateDigit();
	m_digits.at(0).UpdateDigit();
	m_digits.at(2).GetVisualElement().DrawElement(gameWindow);
	m_digits.at(1).GetVisualElement().DrawElement(gameWindow);
	m_digits.at(0).GetVisualElement().DrawElement(gameWindow);
	
	
}



void TimeCounter::AssignCoordinates(const float firstDigitCoordX, const float firstDigitCoordY)
{
	m_digits.at(0).GetVisualElement().AssignCoordinates(firstDigitCoordX, firstDigitCoordY);
	m_digits.at(1).GetVisualElement().AssignCoordinates(firstDigitCoordX + HelperSFML::kCounterDigitOffset, firstDigitCoordY);
	m_digits.at(2).GetVisualElement().AssignCoordinates(firstDigitCoordX + 2 * HelperSFML::kCounterDigitOffset, firstDigitCoordY);
}