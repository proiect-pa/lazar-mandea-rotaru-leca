#include "VisualElement.h"


void VisualElement::LoadTexture(const std::string& filename)
{
	m_elementTexture.loadFromFile(filename);
	m_elementSprite.setTexture(m_elementTexture);

}

void VisualElement::AssignCoordinates(const float coordinateX, const float coordinateY)
{
	m_elementCoordinates.x = coordinateX;
	m_elementCoordinates.y = coordinateY;
}

bool VisualElement::ContainsCoordinates(const int coordinateX, const int coordinateY, const uint8_t buttonHeight, const uint8_t buttonWidth) const
{
	return coordinateX >= m_elementCoordinates.x && coordinateX <= (m_elementCoordinates.x + buttonWidth)
		&& coordinateY >= m_elementCoordinates.y && coordinateY <= (m_elementCoordinates.y + buttonHeight);
}

const sf::Vector2f& VisualElement::GetElementCoordinates() const
{


	return m_elementCoordinates;

}

void VisualElement::DrawElement(sf::RenderWindow& gameWindow)
{
	m_elementSprite.setPosition(m_elementCoordinates);
	gameWindow.draw(m_elementSprite);
}

void VisualElement::SetElementCoordinates(const sf::Vector2f& elementCoordinates)
{
	m_elementCoordinates.x = elementCoordinates.x;
	m_elementCoordinates.y = elementCoordinates.y;
}

sf::Sprite& VisualElement::GetElementSprite()
{
	return std::ref(m_elementSprite);
}

sf::Texture& VisualElement::GetElementTexture()
{
	return std::ref(m_elementTexture);
}


