#pragma once

#include <SFML/Graphics.hpp>
#include "Difficulty.h"
#include <functional>
#include "VisualElement.h"

class IButton
{
public:

	IButton() = default;

	
	
	
	virtual void SetButtonCoordinates(const Difficulty currentGameDifficulty) = 0;
	

};

