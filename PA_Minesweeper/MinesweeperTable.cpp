#include "MinesweeperTable.h"
#include <numeric>
#include <random>


const std::string filePath = "log.txt";
Logger& minesweeperTableLogger = LoggerSingleton::getLoggerInstance(filePath);


MinesweeperTable::MinesweeperTable()
{
	minesweeperTableLogger.Log("MinesweeperTable's constructor has been called!", Logger::Level::Info);
}

void MinesweeperTable::FillTable()
{

	minesweeperTableLogger.Log("FillTable() method has been called.", Logger::Level::Info);
	const auto& [tableHeight, tableWidth] = GetHeightWidth();

	uint16_t customTableWidth = NULL;
	if (m_tableContainer.CheckIfCustomMode()) {
		customTableWidth = m_tableContainer.GetCustomWidth();
	}


	SetHeightWidth();

	uint8_t row = 0u;
	uint8_t column = 0u;

	auto& tableContainer = m_tableContainer.GetContainer();

	for (auto& cell : tableContainer) {

		Cell newCell(row, column);
		cell = std::make_shared<Cell>(newCell);

		++column;
		if (m_tableContainer.CheckIfCustomMode()) {
			if (column == customTableWidth) {
				column = 0u;
				++row;
			}

		}
		else {
			if (column == ToUnderlyingType(tableWidth))
			{
				column = 0u;
				++row;
			}
		}
	}











}




void MinesweeperTable::GenerateBombs()
{
	minesweeperTableLogger.Log("GenerateBombs() method has been called.", Logger::Level::Info);
	

	uint16_t tableHeight, tableWidth, totalNumberOfBombs;
	if (!m_tableContainer.CheckIfCustomMode()) {
		totalNumberOfBombs = ToUnderlyingType(GetCurrentDifficulty());
		const auto& [tableEnumHeight, tableEnumWidth] = GetHeightWidth();
		tableHeight = ToUnderlyingType(tableEnumHeight);
		tableWidth = ToUnderlyingType(tableEnumWidth);
	}

	else {
		tableWidth = m_tableContainer.GetCustomWidth();
		tableHeight = m_tableContainer.GetCustomHeight();
		totalNumberOfBombs = m_tableContainer.GetCustomNoBombs();
	}




	while (totalNumberOfBombs) {


		std::uniform_int_distribution<> rowDistrib(0, tableHeight - 1);
		std::random_device randomDevice;
		std::mt19937 rand(randomDevice());
		auto randomRow = rowDistrib(rand);

		std::uniform_int_distribution<> columnDistrib(0, tableWidth - 1);
		std::random_device randDevice;
		std::mt19937 random(randDevice());
		auto randomColumn = columnDistrib(random);

		if (!isBomb(m_tableContainer, (m_tableContainer.GetElement(randomRow, randomColumn)))) {

			m_tableContainer.GetElement(randomRow, randomColumn)->SetCellType(Cell::CellType::Bomb);
			--totalNumberOfBombs;
		}
	}









}


void MinesweeperTable::IncreaseDifficultyLevel()
{
	switch (GetCurrentDifficulty())
	{
	case Difficulty::Beginner:
		SetCurrentDifficulty(Difficulty::Intermediate);
		break;
	case Difficulty::Intermediate:
		SetCurrentDifficulty(Difficulty::Expert);
		break;
	case Difficulty::Expert:
		SetCurrentDifficulty(Difficulty::Beginner);
		break;
	}


	SetHeightWidth();
}

void MinesweeperTable::DecreaseDifficultyLevel()
{
	switch (GetCurrentDifficulty())
	{
	case Difficulty::Expert:
		SetCurrentDifficulty(Difficulty::Intermediate);
		break;
	case Difficulty::Intermediate:
		SetCurrentDifficulty(Difficulty::Beginner);
		break;
	case Difficulty::Beginner:
		SetCurrentDifficulty(Difficulty::Expert);
		break;
	}


	SetHeightWidth();
}

uint8_t MinesweeperTable::NearbyNoBombs(const std::shared_ptr<Cell>& currentCell) 
{
	//minesweeperTableLogger.Log("NearbyNoBombs() method has been called.", Logger::Level::Info);
	const auto& [line, column] = std::make_pair(currentCell->GetLine(), currentCell->GetColumn());
	std::vector<std::shared_ptr<Cell>> neighbors;
	CellNeighbors(neighbors, this->m_tableContainer, line, column);



	uint8_t partialSum = 0u;
	uint8_t numberOfBombs = std::accumulate(neighbors.begin(), neighbors.end(), partialSum,
		[](uint8_t partialSum, const  std::shared_ptr<Cell>& cellPtr) {
			if (cellPtr->GetCellType() == Cell::CellType::Bomb)
				++partialSum;
			return partialSum;

		}
	);


	return numberOfBombs;
}


void MinesweeperTable::SetHeightWidth()
{

	minesweeperTableLogger.Log("SetHeightWidth() method has been called.", Logger::Level::Info);

	if (!m_tableContainer.CheckIfCustomMode()) {
		const auto& search = TableSize::difficultySizeMap.find(GetCurrentDifficulty());

		const auto [tableHeight, tableWidth] = search->second;

		m_tableContainer.Clear();
		m_tableContainer.Resize(ToUnderlyingType(tableHeight) * ToUnderlyingType(tableWidth));
	}

	else {

		const auto tableHeight = m_tableContainer.GetCustomHeight();
		const auto tableWidth = m_tableContainer.GetCustomWidth();
		m_tableContainer.Clear();
		m_tableContainer.Resize(tableHeight * tableWidth);
	}
	


	


}



const TableContainer& MinesweeperTable::GetTableContainer() const
{
	return this->m_tableContainer;
}



TableContainer& MinesweeperTable::GetTableContainer()
{
	return m_tableContainer;
}

const Difficulty MinesweeperTable::GetCurrentDifficulty() const
{

	return GetTableContainer().GetCurrentDifficulty();
}

void MinesweeperTable::SetCurrentDifficulty(const Difficulty difficultyLevel)
{
	this->m_tableContainer.SetCurrentDifficulty(difficultyLevel);



}





const std::pair<TableSize::TableHeight, TableSize::TableWidth>& MinesweeperTable::GetHeightWidth() const
{

	//minesweeperTableLogger.Log("GetHeightWidth() method has been called.", Logger::Level::Info);
	const auto& sizePair = TableSize::difficultySizeMap.find(GetCurrentDifficulty());

	return sizePair->second;
}

