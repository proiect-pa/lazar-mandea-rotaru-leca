#include "TableSize.h"


const TableSize::sizeMap TableSize::difficultySizeMap = { {Difficulty::Beginner, {TableSize::TableHeight::BeginnerHeight, TableSize::TableWidth::BeginnerWidth}} ,
	{Difficulty::Intermediate, {TableSize::TableHeight::IntermediateHeight, TableSize::TableWidth::IntermediateWidth}},
	{Difficulty::Expert, {TableSize::TableHeight::ExpertHeight, TableSize::TableWidth::ExpertWidth}} };

