#pragma once
#include "IButton.h"
#include "VisualElement.h"

class CustomDifficultyButton : public IButton
{
public:

	 CustomDifficultyButton();
	 void SetButtonCoordinates(const Difficulty currentGameDifficulty);
	 VisualElement& GetVisualElement();
	 const VisualElement& GetVisualElement() const;

private:
	VisualElement m_visualElement;

};

