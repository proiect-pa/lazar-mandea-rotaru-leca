#include "CellHash.h"

size_t CellHash::operator()(const std::shared_ptr<Cell>& cellPtr) const
{

	
	return m_hash(cellPtr->GetLine()) ^ m_hash(cellPtr->GetColumn() >> 1) ;

}
