#pragma once
#include <SFML/Graphics.hpp>
#include <string>
#include "Difficulty.h"

#include "Utility.h"

class HelperSFML
{
public:
	
	enum class DifficultyWindowOffset : uint8_t {


		BeginnerLineOffset = 60,
		BeginnerColumnOffset = 80,
		IntermediateLineOffset = 140,
		IntermediateColumnOffset = 120,
		ExpertLineOffset = 120,
		ExpertColumnOffset = 130


	};



	static void LoadTileTexture(sf::Texture& tileTexture, sf::Sprite& tileSprite);
	static const uint16_t GetLineOffset(const Difficulty);
	static const uint16_t GetColumnOffset(const Difficulty);
	static const std::pair<uint16_t,uint16_t>& GetCustomOffsets(const uint16_t,const uint16_t);


	
	static const uint8_t kDifficultyButtonHeight = 34;
	static const uint8_t kDifficultyButtonWidth = 42;
	static const uint8_t kCustomDifficultyButtonHeight = 60;
	static const uint8_t kCustomDifficultyButtonWidth = 160;
	static const uint16_t kCustomDifficultyMaxHeight = 27;
	static const uint16_t kCustomDifficultyMaxWidth = 54;
	static const uint16_t kCounterDigitOffset = 12;
	










	




};

