#include "TableContainer.h"

#include <memory>
#include <stdexcept>

#include <iterator>
#include "TableSize.h"

#include "Utility.h"


const std::string filePath = "log.txt";
Logger& tableContainerLogger = LoggerSingleton::getLoggerInstance(filePath);





const bool TableContainer::VerifyPositionValidity(const uint8_t line, const uint8_t column)
{
	if (!CheckIfCustomMode()) {
		
		const auto& [tableHeight, tableWidth] = TableSize::difficultySizeMap.find(m_currentDifficultyLevel)->second;
		if (line >= ToUnderlyingType(tableHeight) || column >= ToUnderlyingType(tableWidth))
			return false;
		return true;
	}

	else {
		
		const auto tableHeight = GetCustomHeight();
		const auto tableWidth = GetCustomWidth();
		if (line >= tableHeight || column >= tableWidth)
			return false;
		return true;

	}
}




const Difficulty TableContainer::GetCurrentDifficulty() const
{
	
	return m_currentDifficultyLevel;
}

void TableContainer::SetCurrentDifficulty(const Difficulty difficultyLevel)
{
	this->m_currentDifficultyLevel = difficultyLevel;
}








	void TableContainer::Resize(const uint16_t newSize)
	{
		tableContainerLogger.Log("Table's container has been resized.", Logger::Level::Info);
		m_tableContainer.resize(newSize);
	}

	void TableContainer::Clear()
	{
		tableContainerLogger.Log("Table's container has been cleared.", Logger::Level::Info);
		m_tableContainer.clear();
	}

	std::vector<std::shared_ptr<Cell>>& TableContainer::GetContainer()  {

		return std::ref(m_tableContainer);


	}

	const std::vector<std::shared_ptr<Cell>>& TableContainer::GetContainer() const
	{
		return this->m_tableContainer;
	}

	
	

	TableContainer::TableContainer() : m_currentDifficultyLevel(Difficulty::Beginner), m_customDifficulty(false)
	{

	 tableContainerLogger.Log("TableContainer's constructor has been called!", Logger::Level::Info);
	}

	const std::shared_ptr<Cell>& TableContainer::GetElement(const uint8_t row, const uint8_t column)  const
	{	

		

		for (auto& element : m_tableContainer) {
			const auto& [elementRow, elementColumn] = std::make_pair(element->GetLine(), element->GetColumn());
			if (elementRow == row && elementColumn == column)
				return element;
		}

		return std::make_shared<Cell>(nullptr);

	}


	  std::shared_ptr<Cell>& TableContainer::GetElement(const uint8_t row, const uint8_t column)
	 {
		  
		 for (auto& element : m_tableContainer) {
			 const auto& [elementRow, elementColumn] = std::make_pair(element.get()->GetLine(), element.get()->GetColumn());
			 if (elementRow == row && elementColumn == column)
				 return element;
		 }
	 }



	  void TableContainer::SetCustomMode(const bool customDifficultyMode)
	  {
		  tableContainerLogger.Log("SetCustomMode() method has been called.", Logger::Level::Info);
		  m_customDifficulty = customDifficultyMode;
	  }

	  const bool TableContainer::CheckIfCustomMode()
	  {
		  
		  return m_customDifficulty;
	  }



	

	  void TableContainer::SetCustomModeValues(const uint16_t customHeight, const uint16_t customWidth, const uint16_t customNoBombs)
	  {
		  m_customModeValues.at(0).emplace() = customHeight;
		  m_customModeValues.at(1).emplace() = customWidth;
		  m_customModeValues.at(2).emplace() = customNoBombs;
	  }

	  const uint16_t TableContainer::GetCustomNoBombs() const
	  {
		  return m_customModeValues.at(2).value();
	  }

	  const uint16_t TableContainer::GetCustomHeight() const
	  {
		  return m_customModeValues.at(0).value();
	  }

	  const uint16_t TableContainer::GetCustomWidth() const
	  {
		  return m_customModeValues.at(1).value();
	  }
