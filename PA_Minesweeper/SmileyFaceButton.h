#pragma once
#include "IButton.h"

class SmileyFaceButton : public IButton
{
public:
	SmileyFaceButton();
	void SetButtonCoordinates(const Difficulty currentGameDifficulty);
	void SetHappyReaction();
	void SetLossReaction();
	void SetWinReaction();
	void SetClickReaction();
	VisualElement& GetVisualElement();
	const VisualElement& GetVisualElement() const;



private:
	const uint8_t reactionTileSize = 32u;
	VisualElement m_visualElement;


};

