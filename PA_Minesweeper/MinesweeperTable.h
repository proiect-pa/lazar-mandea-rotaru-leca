#pragma once

#include <vector>
#include "Cell.h"
#include "Difficulty.h"
#include <memory>
#include "Utility.h"
#include <unordered_map>
#include "TableSize.h"
#include <algorithm>
#include <unordered_set>
#include "TableContainer.h"
#include <queue>
#include <iostream>
#include <optional>
#include "CellHash.h"
#include "EqualFunctionCell.h"


class MinesweeperTable
{




public:

	MinesweeperTable();
	void FillTable();
	void GenerateBombs();

	void IncreaseDifficultyLevel();
	void DecreaseDifficultyLevel();


	uint8_t NearbyNoBombs(const std::shared_ptr<Cell>&);

	void SetHeightWidth();


	TableContainer& GetTableContainer();
	const TableContainer& GetTableContainer() const;



	const Difficulty GetCurrentDifficulty() const;
	void SetCurrentDifficulty(const Difficulty);



	const std::pair<TableSize::TableHeight, TableSize::TableWidth>& GetHeightWidth() const;







private:


	 TableContainer m_tableContainer;


};

inline bool ValidPosition(TableContainer& tableContainer, const uint8_t line, const uint8_t column) {


	return tableContainer.VerifyPositionValidity(line, column);


}


inline void CellNeighbors(std::vector<std::shared_ptr<Cell>>& neighbors, TableContainer& tableContainer, const uint8_t line, const uint8_t column) {


	if (ValidPosition(tableContainer, line - 1, column))
		neighbors.emplace_back(tableContainer.GetElement(line - 1, column));

	if (ValidPosition(tableContainer, line - 1, column + 1))
		neighbors.emplace_back(tableContainer.GetElement(line - 1, column + 1));

	if (ValidPosition(tableContainer, line - 1, column - 1))
		neighbors.emplace_back(tableContainer.GetElement(line - 1, column - 1 ));

	if (ValidPosition(tableContainer, line, column + 1))
		neighbors.emplace_back(tableContainer.GetElement(line, column + 1));

	if (ValidPosition(tableContainer, line, column - 1))
		neighbors.emplace_back(tableContainer.GetElement(line, column - 1));

	if (ValidPosition(tableContainer, line + 1, column))
		neighbors.emplace_back(tableContainer.GetElement(line + 1, column));

	if (ValidPosition(tableContainer, line + 1, column + 1))
		neighbors.emplace_back(tableContainer.GetElement(line + 1, column + 1));

	if (ValidPosition(tableContainer, line + 1, column - 1))
		neighbors.emplace_back(tableContainer.GetElement(line + 1, column - 1));





}


inline bool PushableInBFS(MinesweeperTable& minesweeperTable, const std::shared_ptr<Cell>& cellToBePushed) {


	return minesweeperTable.NearbyNoBombs(cellToBePushed) == 0;


}


inline void HelperFunctionBFS(MinesweeperTable& minesweeperTable, const uint16_t line, const uint16_t column, std::queue<std::shared_ptr<Cell>>& queueBFS,
	std::unordered_set<std::shared_ptr<Cell>, CellHash, EqualFunctionCell>& visitedCells, std::vector<std::vector<uint8_t>>& gridRepresentation
	, uint16_t& m_cellsToDiscover) {

	 auto& tableContainer = minesweeperTable.GetTableContainer();


	if (tableContainer.VerifyPositionValidity(line, column)) {
		const auto& currentCell = tableContainer.GetElement(line, column);

		if (!currentCell->CheckIfFlagged() && currentCell->GetCellType() != Cell::CellType::Bomb) {
			
			if (!visitedCells.contains(currentCell)) {
				
				
				const auto& numberOfBombs = minesweeperTable.NearbyNoBombs(currentCell);
				if (PushableInBFS(minesweeperTable, currentCell)) {
					
					queueBFS.emplace(currentCell);
					gridRepresentation.at(line).at(column) = numberOfBombs;




				}


				else {

					if (gridRepresentation.at(line).at(column) == 10) {
						gridRepresentation.at(line).at(column) = numberOfBombs;
						visitedCells.emplace(currentCell);
						//--m_cellsToDiscover;
					}



				}

			}

		}

	}
}




inline  void BFS(MinesweeperTable& minesweeperTable, const std::shared_ptr<Cell>& cell, std::vector<std::vector<uint8_t>>& gridRepresentation, uint16_t& m_cellsToDiscover) {

	std::queue<std::shared_ptr<Cell>> queueBFS;
	std::unordered_set<std::shared_ptr<Cell>, CellHash, EqualFunctionCell> visitedCells;



	auto& tableContainer = minesweeperTable.GetTableContainer();


	queueBFS.emplace(cell);

	auto numberOfBombs = minesweeperTable.NearbyNoBombs(cell);
	gridRepresentation.at(cell->GetLine()).at(cell->GetColumn()) = numberOfBombs;





	while (!queueBFS.empty()) {

		const auto& currentCell = queueBFS.front();
		queueBFS.pop();
		const auto [line, column] = std::make_pair(currentCell->GetLine(), currentCell->GetColumn());


		if (!visitedCells.contains(currentCell)) {
			visitedCells.emplace(currentCell);
			HelperFunctionBFS(minesweeperTable, line, column + 1, queueBFS, visitedCells, gridRepresentation, m_cellsToDiscover);
			HelperFunctionBFS(minesweeperTable, line, column - 1, queueBFS, visitedCells, gridRepresentation, m_cellsToDiscover);
			HelperFunctionBFS(minesweeperTable, line + 1, column + 1, queueBFS, visitedCells, gridRepresentation, m_cellsToDiscover);
			HelperFunctionBFS(minesweeperTable, line + 1, column, queueBFS, visitedCells, gridRepresentation, m_cellsToDiscover);
			HelperFunctionBFS(minesweeperTable, line + 1, column - 1, queueBFS, visitedCells, gridRepresentation, m_cellsToDiscover);
			HelperFunctionBFS(minesweeperTable, line - 1, column + 1, queueBFS, visitedCells, gridRepresentation, m_cellsToDiscover);
			HelperFunctionBFS(minesweeperTable, line - 1, column, queueBFS, visitedCells, gridRepresentation, m_cellsToDiscover);
			HelperFunctionBFS(minesweeperTable, line - 1, column - 1, queueBFS, visitedCells, gridRepresentation, m_cellsToDiscover);
			
		}


		
		


	}

	m_cellsToDiscover -= (visitedCells.size());



}