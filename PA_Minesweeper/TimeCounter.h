#pragma once
#include "ICounter.h"
#include "VisualElement.h"
#include "HelperSFML.h"

class TimeCounter : public ICounter
{

public:

	
	void TickUp();
	
	void SetCounterCoordinates(const Difficulty);
	void StartClock();
	void ResetClock();
	const bool TimeIsUp();
	const bool IncreaseIndex(const uint8_t);
	sf::Clock& GetClock();
	const sf::Clock& GetClock() const;
	void DrawDigits(sf::RenderWindow&);
	void AssignCoordinates(const float, const float);

	 
private:
	sf::Clock m_clock;
	std::array<CounterDigit, 3> m_digits;

};

