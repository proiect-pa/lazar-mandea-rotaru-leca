#include "HelperSFML.h"

#include "../Logging/LoggerSingleton.h"


const std::string filePath = "log.txt";
Logger& helperFunctionsLogger = LoggerSingleton::getLoggerInstance(filePath);


void HelperSFML::LoadTileTexture(sf::Texture& tileTexture, sf::Sprite& tileSprite){




	tileTexture.loadFromFile("images/tiles.png");
	tileSprite.setTexture(tileTexture);

	helperFunctionsLogger.Log("The sprite used for keeping the tiles updated has been loaded.", Logger::Level::Info);

}

const uint16_t HelperSFML::GetLineOffset(const Difficulty currentDifficultyLevel)
{



	switch (currentDifficultyLevel) {
	case Difficulty::Beginner :
		return ToUnderlyingType(DifficultyWindowOffset::BeginnerLineOffset);
		
	case Difficulty::Intermediate:
		return ToUnderlyingType(DifficultyWindowOffset::IntermediateLineOffset);
		
	case Difficulty::Expert:
		return ToUnderlyingType(DifficultyWindowOffset::ExpertLineOffset);
		

	}
}

const uint16_t HelperSFML::GetColumnOffset(const Difficulty currentDifficultyLevel)
{
	

	switch (currentDifficultyLevel) {
	case Difficulty::Beginner:
		return ToUnderlyingType(DifficultyWindowOffset::BeginnerColumnOffset);

	case Difficulty::Intermediate:
		return ToUnderlyingType(DifficultyWindowOffset::IntermediateColumnOffset);

	case Difficulty::Expert:
		return ToUnderlyingType(DifficultyWindowOffset::ExpertColumnOffset);


	}
}

const std::pair<uint16_t,uint16_t>& HelperSFML::GetCustomOffsets(const uint16_t initialHeight,const  uint16_t initialWidth)
{
	
	
	 uint16_t customHeight;

	 uint16_t customWidth;

	if (initialHeight < 10) {

		customHeight = 400;

	}

	else if (initialHeight < 20) {
		customHeight = 280;

	}

	else {
		customHeight = 80;

	}


	if (initialWidth < 10) {

		customWidth = 820;
	}

	else if (initialWidth < 20) {
		customWidth = 700;

		

	}

	else if (initialWidth < 30) {
		customWidth = 550;

	}

	else if (initialWidth < 40) {
		customWidth = 400;

	}
	else if (initialWidth < 50) {
		customWidth = 300;

	}

	else {
		customWidth = 100;

	}



	return std::make_pair(customHeight, customWidth);
	
}



