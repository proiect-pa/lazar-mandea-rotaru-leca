#include "CounterDigit.h"

CounterDigit::CounterDigit() : m_digitIndex(0u)
{
	m_visualElement.LoadTexture("images/digits.png");
	UpdateDigit();
}



VisualElement& CounterDigit::GetVisualElement()
{
	return std::ref(m_visualElement);
}

const VisualElement& CounterDigit::GetVisualElement() const
{
	return m_visualElement;
}

const uint8_t CounterDigit::GetDigitIndex() const
{
	return m_digitIndex;
}

void CounterDigit::IncreaseDigitIndex()
{
	++m_digitIndex;
}



void CounterDigit::SetDigitIndexNull()
{
	m_digitIndex = 0u;
}

const bool CounterDigit::HasMaxValue()
{
	return m_digitIndex == 9;
}

const bool CounterDigit::HasMinValue()
{
	return m_digitIndex == 0;
}

void CounterDigit::DecreaseDigitIndex()
{
	--m_digitIndex;
}

void CounterDigit::UpdateDigit()
{
	
	m_visualElement.GetElementSprite().setTextureRect(sf::IntRect(m_digitIndex * kDigitWidth, 0, kDigitWidth, kDigitHeight));
}

void CounterDigit::SetDigitIndex(const uint8_t digitIndex)
{
	m_digitIndex = digitIndex;
}


