#include "SmileyFaceButton.h"

SmileyFaceButton::SmileyFaceButton()
{
	m_visualElement.LoadTexture("images/reactions.png");
	SetHappyReaction();
}

void SmileyFaceButton::SetButtonCoordinates(const Difficulty currentGameDifficulty)
{
	switch (currentGameDifficulty) {
	case Difficulty::Beginner:


		m_visualElement.AssignCoordinates(190, 20);

		break;
	case Difficulty::Intermediate:

		m_visualElement.AssignCoordinates(360, 50);

		break;
	case Difficulty::Expert:

		m_visualElement.AssignCoordinates(550, 60);


		break;


	}
}

void SmileyFaceButton::SetHappyReaction()
{
	auto& buttonSprite = m_visualElement.GetElementSprite();
	buttonSprite.setTextureRect(sf::IntRect(0, 0, reactionTileSize, reactionTileSize));

}

void SmileyFaceButton::SetLossReaction()
{
	auto& buttonSprite = m_visualElement.GetElementSprite();
	buttonSprite.setTextureRect(sf::IntRect(3 * reactionTileSize, 0, reactionTileSize, reactionTileSize));
}

void SmileyFaceButton::SetWinReaction()
{
	auto& buttonSprite = m_visualElement.GetElementSprite();
	buttonSprite.setTextureRect(sf::IntRect(2 * reactionTileSize, 0, reactionTileSize, reactionTileSize));
}

void SmileyFaceButton::SetClickReaction()
{
	auto& buttonSprite = m_visualElement.GetElementSprite();
	buttonSprite.setTextureRect(sf::IntRect(1 * reactionTileSize, 0, reactionTileSize, reactionTileSize));
}


VisualElement& SmileyFaceButton::GetVisualElement()
{
	return std::ref(m_visualElement);
}

const VisualElement& SmileyFaceButton::GetVisualElement() const
{
	return m_visualElement;
}