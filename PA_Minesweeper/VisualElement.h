#pragma once

#include <SFML/Graphics.hpp>

class VisualElement {

public:

	void LoadTexture(const std::string&);
	void AssignCoordinates(const float, const float);
	bool ContainsCoordinates(const int, const int, const uint8_t, const uint8_t) const;
	const sf::Vector2f& GetElementCoordinates() const;
	void DrawElement(sf::RenderWindow&);
	void SetElementCoordinates(const sf::Vector2f&);
	sf::Sprite& GetElementSprite();
	sf::Texture& GetElementTexture();
	

private:

	sf::Vector2f m_elementCoordinates;
	sf::Sprite m_elementSprite;
	sf::Texture m_elementTexture;


};