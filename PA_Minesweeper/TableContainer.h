#pragma once

#include <vector>
#include "Cell.h"
#include "Difficulty.h"
#include "../Logging/LoggerSingleton.h"
#include <optional>
#include <array>



class TableContainer
{


public:
	
	TableContainer();



	const std::shared_ptr<Cell>& GetElement(const uint8_t, const uint8_t) const;

	std::shared_ptr<Cell>& GetElement(const uint8_t, const uint8_t);
	

	void Resize(const uint16_t);
	void Clear();
	std::vector<std::shared_ptr<Cell>>& GetContainer();
	const std::vector<std::shared_ptr<Cell>>& GetContainer() const;
	
	const Difficulty GetCurrentDifficulty() const;
	void SetCurrentDifficulty(const Difficulty);



	const bool VerifyPositionValidity(const uint8_t line, const uint8_t column);


	void SetCustomModeValues(const uint16_t, const uint16_t, const uint16_t);
	const uint16_t GetCustomNoBombs() const;
	const uint16_t GetCustomHeight() const;
	const uint16_t GetCustomWidth() const;
	void SetCustomMode(const bool);
	const bool CheckIfCustomMode();

	


private:

	Difficulty m_currentDifficultyLevel;
	std::array<std::optional<uint16_t>, 3> m_customModeValues;
	std::vector<std::shared_ptr<Cell>> m_tableContainer;
	bool m_customDifficulty;

};





inline bool isBomb(const TableContainer& minesweeperTable, const std::shared_ptr<Cell>& cell) {



	const auto& [line, column] = std::make_pair(cell->GetLine(), cell->GetColumn());

	return minesweeperTable.GetElement(line, column)->GetCellType() == Cell::CellType::Bomb;

}