

#include "pch.h"
#include "CppUnitTest.h"
#include <numeric>
#include <functional>


#include "../PA_Minesweeper/Difficulty.h"
#include "../PA_Minesweeper/HelperSFML.cpp"



using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTesting
{
	TEST_CLASS(HelperTests)
	{
	public:

		TEST_METHOD(GetLineOffsetMethodTest) {

			Assert::IsTrue(HelperSFML::GetLineOffset(Difficulty::Beginner) == 60 &&
				HelperSFML::GetLineOffset(Difficulty::Intermediate) == 140 &&
				HelperSFML::GetLineOffset(Difficulty::Expert) == 120);


		}



		TEST_METHOD(GetColumnOffsetMethodTest) {

			Assert::IsTrue(HelperSFML::GetColumnOffset(Difficulty::Beginner) == 80 &&
				HelperSFML::GetColumnOffset(Difficulty::Intermediate) == 120 &&
				HelperSFML::GetColumnOffset(Difficulty::Expert) == 130);


		}


		TEST_METHOD(ToUnderlyingTypeInlineMethodTest) {
			Difficulty testDifficulty = Difficulty::Expert;
			Assert::IsTrue(ToUnderlyingType(testDifficulty) == ToUnderlyingType(Difficulty::Expert));



		}



	};
}
