

#include "pch.h"
#include "CppUnitTest.h"
#include <numeric>
#include <functional>
#include <algorithm>


#include "../PA_Minesweeper/MinesweeperTable.cpp"




//#include "../PA_Minesweeper/CellType.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTesting
{
	TEST_CLASS(MinesweeperTableTests)
	{
	public:

		TEST_METHOD(GetHeightWidthMethodTest)
		{
			MinesweeperTable minesweeperTable;
			Assert::IsTrue(minesweeperTable.GetHeightWidth() == std::make_pair(TableSize::TableHeight::BeginnerHeight, TableSize::TableWidth::BeginnerWidth));

		}



		TEST_METHOD(FillTableMethodTest)
		{
			MinesweeperTable minesweeperTable;
			minesweeperTable.FillTable();
			const auto& tableContainer = minesweeperTable.GetTableContainer().GetContainer();


			Assert::IsTrue(std::all_of(tableContainer.begin(), tableContainer.end(), [](const std::shared_ptr<Cell>& cellPtr) {

				return cellPtr->GetCellType() != Cell::CellType::Bomb;

				}));
		}

		TEST_METHOD(GenerateBombsMethodTest)
		{
			MinesweeperTable minesweeperTable;
			minesweeperTable.FillTable();
			minesweeperTable.GenerateBombs();
			auto tableContainer = minesweeperTable.GetTableContainer().GetContainer();

			auto partialSum = 0u;

			uint8_t sum = std::accumulate(tableContainer.begin(), tableContainer.end(), partialSum,
				[](uint8_t partialSum, const  std::shared_ptr<Cell>& cellPtr) {

					partialSum += ToUnderlyingType(cellPtr.get()->GetCellType());
					return partialSum;

				}
			);


			Assert::AreEqual(ToUnderlyingType(minesweeperTable.GetCurrentDifficulty()), sum);


		}


		TEST_METHOD(IncreaseDifficultyLevelMethodTest)
		{
			MinesweeperTable minesweeperTable;

			minesweeperTable.IncreaseDifficultyLevel();





			Assert::IsTrue(minesweeperTable.GetCurrentDifficulty() == Difficulty::Intermediate);

		}



		TEST_METHOD(DecreaseDifficultyLevelMethodTest) {
			MinesweeperTable minesweeperTable;
			minesweeperTable.DecreaseDifficultyLevel();
			Assert::IsTrue(minesweeperTable.GetCurrentDifficulty() == Difficulty::Expert);

		}



	};
}
