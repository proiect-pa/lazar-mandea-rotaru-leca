#include "pch.h"
#include "CppUnitTest.h"






#include "../PA_Minesweeper/FlagCounter.cpp"
#include "../PA_Minesweeper/VisualElement.cpp"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTesting
{
	TEST_CLASS(VisualElementTests)
	{
	public:





		TEST_METHOD(VisualElementAssignCoordinatesTest) {

			VisualElement visualElement;
			visualElement.AssignCoordinates(200, 200);
			const auto& elementCoordinates = visualElement.GetElementCoordinates();
			Assert::IsTrue(elementCoordinates.x == 200 && elementCoordinates.y == 200);
		}


		TEST_METHOD(FlagCounterConstructorTest) {
			FlagCounter flagCounter;
			Assert::IsTrue(!flagCounter.CheckIfNegativeSign());


		}


	};
}
