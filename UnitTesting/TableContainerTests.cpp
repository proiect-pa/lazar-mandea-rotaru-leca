

#include "pch.h"
#include "CppUnitTest.h"
#include <numeric>
#include <functional>


#include "../PA_Minesweeper/TableSize.cpp"

#include "../PA_Minesweeper/TableContainer.cpp"



using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTesting
{
	TEST_CLASS(TableContainerTests)
	{
	public:

		TEST_METHOD(DifficultySizeMapTest) {


			TableSize::sizeMap testMap = { {Difficulty::Beginner, {TableSize::TableHeight::BeginnerHeight, TableSize::TableWidth::BeginnerWidth}} ,
		   {Difficulty::Intermediate, {TableSize::TableHeight::IntermediateHeight, TableSize::TableWidth::IntermediateWidth}},
		   {Difficulty::Expert, {TableSize::TableHeight::ExpertHeight, TableSize::TableWidth::ExpertWidth}} };

			Assert::IsTrue(TableSize::difficultySizeMap == testMap);


		}

		TEST_METHOD(VerifyPositionValidityMethodTest) {


			TableContainer tableContainer;
			Assert::IsFalse(tableContainer.VerifyPositionValidity(500, 5));

		}


		TEST_METHOD(TableContainerConstructorTest) {

			TableContainer tableContainer;
			Assert::IsTrue(tableContainer.GetCurrentDifficulty() == Difficulty::Beginner);


		}



	};
}
