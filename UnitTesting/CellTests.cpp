

#include "pch.h"
#include "CppUnitTest.h"
#include <numeric>
#include <functional>



#include "../PA_Minesweeper/Cell.cpp"














//#include "../PA_Minesweeper/CellType.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTesting
{
	TEST_CLASS(CellTests)
	{
	public:

		TEST_METHOD(CellConstructorFirstTest) {

			Cell testCell(5, 2);

			Assert::IsTrue(testCell.GetCellType() == Cell::CellType::Clear);

		}

		TEST_METHOD(CellFlagSetterGetterTest) {

			Cell testCell(2, 3);
			testCell.SetAsFlagged();
			Assert::IsTrue(testCell.CheckIfFlagged());
		}

		TEST_METHOD(CellCopyConstructorTest) {

			Cell testCell(5, 2);

			Cell copyCell(testCell);


			Assert::IsTrue((copyCell.GetLine() == testCell.GetLine() && copyCell.GetColumn() == testCell.GetColumn()));

		}



	};
}
