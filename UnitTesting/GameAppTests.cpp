

#include "pch.h"
#include "CppUnitTest.h"
#include <numeric>
#include <functional>




#include "../PA_Minesweeper/GameApp.cpp"
#include "../PA_Minesweeper/IButton.h"
#include "../PA_Minesweeper/ICounter.h"
#include "../PA_Minesweeper/SmileyFaceButton.cpp"

#include "../PA_Minesweeper/IncreaseDifficultyButton.cpp"

#include "../PA_Minesweeper/DecreaseDifficultyButton.cpp"

#include "../PA_Minesweeper/CustomDifficultyButton.cpp"

#include "../PA_Minesweeper/CounterDigit.cpp"
#include "../PA_Minesweeper/TimeCounter.cpp"

#include <SFML/Audio.hpp>
#include "../Logging/LoggerSingleton.cpp"
#include "../Logging/Logger.cpp"
#include "../PA_Minesweeper/CellHash.cpp"
#include "../PA_Minesweeper/EqualFunctionCell.h"







//#include "../PA_Minesweeper/CellType.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTesting
{
	TEST_CLASS(GameAppTests)
	{
	public:

		TEST_METHOD(GameOverMethodTest) {

			GameApp gameApp;
			Assert::IsFalse(gameApp.GameOver() == true);

		}



		TEST_METHOD(GameAppConstructorTest) {

			GameApp gameApp;

			Assert::IsTrue(gameApp.GetCurrentGameState() == GameApp::GameState::Ongoing);

		}

		TEST_METHOD(MouseCoordinatesAdjustmentTest) {
			GameApp gameApp;
			uint16_t mouseCoordX = 50u, mouseCoordY = 70u;
			auto copyMouseCoordX = mouseCoordX;
			auto copyMouseCoordY = mouseCoordY;
			sf::RenderWindow renderWindow(sf::VideoMode(800, 600), "Test Window");
			gameApp.AdjustMouseCoordinates(mouseCoordX, mouseCoordY, renderWindow);

			Assert::IsTrue(mouseCoordX != copyMouseCoordX && mouseCoordY != copyMouseCoordY);

		}


		TEST_METHOD(SetCustomModeMethodTest) {
			GameApp gameApp;

			gameApp.SetCustomMode(true);
			Assert::IsTrue(gameApp.CheckIfCustomMode());


		}

		TEST_METHOD(AdjustScreenSizeMethodTest) {
			GameApp gameApp;
			sf::RenderWindow renderWindow(sf::VideoMode(800, 600), "Test Window");
			gameApp.AdjustScreenSize(renderWindow);

			const auto& windowSize = renderWindow.getSize();
			Assert::IsTrue(windowSize.x == 400 && windowSize.y == 400);


		}


		TEST_METHOD(ValidMouseCoordinatesMethodTest) {
			GameApp gameApp;
			Assert::IsFalse(gameApp.ValidMouseCoordinates(0, 25));


		}


		TEST_METHOD(UncoveredCellMethodTest) {

			GameApp gameApp;
			const auto& tableContainer = gameApp.GetMinesweeperTable().GetTableContainer().GetContainer();

			for (const auto& cell : tableContainer) {

				if (!gameApp.UncoveredCell(cell->GetLine(), cell->GetColumn()))
					Assert::Fail();
			}
		}


		TEST_METHOD(StartNewGameMethodTest) {
			GameApp gameApp;
			gameApp.PlaceNumberOfBombsTexture(5, 5);
			gameApp.StartNewGame();
			const auto& tableContainer = gameApp.GetMinesweeperTable().GetTableContainer().GetContainer();

			for (const auto& cell : tableContainer) {
				if (gameApp.UncoveredCell(cell->GetLine(), cell->GetColumn())) {
					Assert::Fail;

				}


			}


		}



	};
}
